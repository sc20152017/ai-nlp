<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nlp extends CI_Controller {
        
    function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('text');
		$this->load->helper('url');
		$this->load->model('');	
	}
        
	/**
	* Nlp.php
	* Diinginkan sistem pengolah bahasa alami untuk keperluan melayani pencarian buku/jurnal/prosiding di perpustakaan
    * a. Buatlah production rules untuk keperluan diatas, tanpa membatasi nama buku/jurnal/prosiding yang dicari
	* b. Jika pola kalimat yang diujudkan sebagai production rules sudah anda tentukan. Rancanglah bagaimana anda menyimpan pola2 kalimat tersebut.
    * c. Buatlah program scanner untuk mengetes kalimat input apakah sesuai dengan kosakata tersimpan
	* d. Buatlah program parser untuk mengetes struktur kalimat input apakah sesuai dengan pola kalimat yang tersimpan
	*
	*/
    

	/*Definisi pola*/
	private $list_pola = array(
		array(
			"code"		=> "pola_1",
			"rule"		=> "<kata_cari> <jenis> <kata_kunci>",
			"deskripsi"	=> "Mencari jenis tulisan (buku / jurnal / prosiding) dengan kata kunci tertentu"
			),
		array(
			"code"		=> "pola_2",
			"rule"		=> "<kata_cari> <jenis> <kata_karangan> <kata_kunci_pengarang>",
			"deskripsi"	=> "Mencari jenis tulisan (buku / jurnal / prosiding) dengan kata kunci pengarang / penulis tertentu"
			),
                array(
			"code"		=> "pola_3",
			"rule"		=> "<kata_cari> <jenis> <kata_kerja> <kata_kunci>",
			"deskripsi"	=> "Mencari jenis tulisan (buku / jurnal / prosiding) dengan kata kerja diikuti kata kunci tertentu"
			),
                array(
			"code"		=> "pola_4",
			"rule"		=> "<kata_cari> <jenis> <kata_waktu> <waktu>",
			"deskripsi"	=> "Mencari jenis tulisan (buku / jurnal / prosiding) dengan keterangan waktu tertentu"
			)
	);

	private  $list_variable = array(
		"<kata_cari>" 	=> "/cari|tampilkan|mencari|menampilkan/",
		"<jenis>" 	=> "/buku|prosiding|jurnal/",
		"<kata_kerja>" 	=> "/judul|dengan judul|berjudul/",
                "<kata_waktu>"  => "/tahun|tahun terbit|terbitan|edisi/",
		"<kata_karangan>"=> "/karangan|ditulis|dikarang|ditulis oleh|dikarang oleh|dengan penulis|yang ditulis|yang dikarang/",
		"<kata_kunci_pengarang>"=> "/(?!karangan)(?!berjudul)(?!judul)(?!tulisan)(?!buku)^[a-z\s]*$/",
		"<kata_kunci>"=> "/(?!karangan)(?!berjudul)(?!judul)(?!tahun)(?!terbitan)(?!tulisan)(?!ditulis)(?!ditulis\soleh)(?!yang\sdikarang)(?!dikarang\soleh)(?!dikarang)(?!dengan\spenulis)^[a-z0-9\s]*$/",
                "<waktu>"=> "/(?!karangan)(?!berjudul)(?!judul)(?!tulisan)(?!ditulis)(?!ditulis\soleh)(?!yang\sdikarang)(?!dikarang\soleh)(?!dikarang)(?!dengan\spenulis)^[0-9\s]*$/"
            );


	/*Definisi kosa kata*/
	private  $kosakata = array(
		'index' => 'data'
	);


	public function coba_regex()
	{
		$rule = "/^[a-z0-9\s]*$/";
		$input = "biologie 3";
		$output_array = array();
		preg_match($rule,$input,$output_array);
		print_r($output_array);
		
	}

	public function index()
	{
		$data = array();
		if($this->input->post('kalimat')){
			$kalimat_input = $this->input->post('kalimat');
			$hasil_scanner = $this->_scanner($kalimat_input);
			
			if(!empty($hasil_scanner)){
				$data['kalimat_input'] = $kalimat_input;
				$data['hasil_scanner'] = $hasil_scanner;
				$data['hasil_parser'] = $this->_parser($kalimat_input, $hasil_scanner);
				//print_r($data['hasil_parser']);
			}else{
			}
		}else{

		}

        $this->load->view('nlp',$data);
	}


	/*
	* Parser
	* Fungsi untuk mengetes kalimat input apakah sesuai dengan kosakata tersimpan
	*/
	function _parser($kalimat_input, $hasil_scanner)
	{
		$hasil_parser = array();
		//print_r(array("kalimat_input"=>$kalimat_input));
		//print_r(array("hasil scanner"=>$hasil_scanner));
		foreach ($this->list_pola as $key_pola => $pola) {
			$pecah_pola = (explode(" ",$pola['rule']));
			//print_r(array("pecah_pola"=>$pecah_pola));
			foreach ($pecah_pola as $key_var => $var) {
				//cek apakah $var merupakan <variabel>
				if(substr($var, 0,1)=="<" && substr($var, strlen($var)-1, 1 )==">"){
					//cek apakah $var ada di list variabel
					if(isset($this->list_variable[$var])){
						$var_rule = $this->list_variable[$var];
						//print_r(array('rule'=>$var_rule));
						if(isset($hasil_scanner[$key_var])){
							if(preg_match($var_rule, $hasil_scanner[$key_var]['kata'])){
								//echo "ketemu";
								$output_array = array();
								preg_match($var_rule, $hasil_scanner[$key_var]['kata'],$output_array);
								//print_r($output_array);
							}else{
								break;
							}
						}else{
							break;
						}
					}
				}
				if($key_var == count($pecah_pola)-1){
					//echo "cocok dengan pola : " .$pola['code'];
					$hasil_parser[] = $pola;
				}	
			}
			//break;
		}
		return $hasil_parser;
	}

	/*
	* Scanner
	* Fungsi untuk mengetes struktur kalimat input apakah sesuai dengan pola kalimat yang tersimpan
	*/
	function _scanner($kalimat)
	{
		$filetoken = file(base_url('assets/token.txt'));
		//print_r($filetoken);
        $tahap_1 = strtolower($kalimat);               
        $tahap_2 = preg_replace("/[^ \w]+/","",$tahap_1);                                
        $tahap_3 = (explode(" ",$tahap_2));
        //print_r($tahap_3);
        foreach ($tahap_3 as $key => $token){
            foreach($filetoken as $daftar)
            {
            	//$data[$key]['kata'] = $token;
              	if(strpos($daftar, $token) !== false){
	               // echo $token." ada dalam daftar token";
	               // echo "<br>";
	                $data[$key]['kata']= $token;
	                $data[$key]['status']= 1;
	                break;
              	}else{
	                $data[$key]['kata']= $token;
	                $data[$key]['status']= 0;
             	}
            }
        } 

		return $data;
	} 
}
