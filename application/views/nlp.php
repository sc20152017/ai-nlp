 <?php
$this->load->view('/template/header');
?>
    <style type="text/css">
        .red{
            color: red;
        }
        .blue{
            color: blue;
        }

    </style>
	<section id="home">
		<div class="container">
			<div class="home_wrap center">
				<h1>Masukkan kalimat Anda :</h1>
				<form action="nlp#hasil" method="post">
					<input type="text" name="kalimat" value="" placeholder="Contoh : Cari jurnal karangan Javier" />	

					<div class="submit_wrap"><input type="submit" value="Olah Kalimat" /></div>
					<div class="clear"></div>
				</form>
			</div>
		</div>
	</section>
		
	<section id="hasil">
		<div class="container">
            <br>
            <h2 class="center italic"><?php echo !empty($kalimat_input) ? "Kalimat : \"" .$kalimat_input ."\"" : 'Belum ada kalimat yang diinputkan !' ?></h2>
            <?php if(!empty($kalimat_input)) : ?>
            <h3 class="center">Hasil Scanning</h3>
            <?php if(!empty($hasil_scanner)) : ?>
                <table class="table table-striped table-bordered table-hover table-condensed">
                    <tr><th>Daftar kata</th><th>Status</th></tr>
                    <?php foreach ($hasil_scanner as $key => $item) : ?>
                        <tr>
                            <td><?php echo $item['kata'];?></td>
                            <td class="<?php echo $item['status'] ? 'blue' : 'red'  ?>">
                                <?php echo $item['status'] ? 'Kata ada di dalam token' : 'Kata tidak ada di dalam daftar token' ?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                </table>
            <?php else : ?>

            <?php endif;?>

            <h3 class="center">Hasil Parsing</h3>
            <h2 class="center italic"><?php echo !empty($hasil_parser) ? "" : 'Pola kalimat tidak ditemukan, ulangi lagi!' ?></h2>
            <?php if(!empty($hasil_parser)) : ?>
                <p>Kalimat cocok dengan pola :</p>
                <?php foreach ($hasil_parser as $key => $parser) : ?>
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <tr><th>Kode </th><td><?php echo $parser['code'];?></td></tr>
                        <tr><th>Rule </th><td><?php echo htmlentities($parser['rule']);?></td></tr>
                        <tr><th>Deskripsi </th><td><?php echo $parser['deskripsi'];?></td></tr>
                    </table>
                <?php endforeach;?>
                <?php endif;?>
            <?php endif;?>
		</div>
	</section>
		
	<section id="about">	
                <div class="testimonials_wrap">
                    <div class="container">
                        <center><h1 style="color:white">ANGGOTA TIM</h1></center>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 padbot30">
                                <div class="testim_item">
                                        <a class="testim_author" href="javascript:void(0);"><img src="<?php echo base_url();?>assets/images/Kifni.jpg" alt="" /></a>
                                        <h4 class="testim_name">Kifni Taufik D.</h4>
                                        <p>NIM : 15/388485/PPA/04924</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 padbot30">
                                    <div class="testim_item">
                                            <a class="testim_author" href="javascript:void(0);"><img src="<?php echo base_url();?>assets/images/Nurcahya.jpg" alt="" /></a>
                                            <h4 class="testim_name">Nurcahya Pradana T.P.</h4>
                                            <p>NIM : 15/388492/PPA/04931</p>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
	</section>
<?php
$this->load->view('template/footer');
?>